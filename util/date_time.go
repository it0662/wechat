/*
   Copyright 2020 XiaochengTech

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package util

import (
	"time"
)

// 格式化时间，按照yyyyMMddHHmmss格式
func FormatDateTime(t time.Time) string {
	return t.Format("20060102150405")
}

// 将时间转换为时间戳
func ConvertDateTimeToUnix(timeStr string) (timestamp int64, err error) {
	t, err := time.ParseInLocation("20060102150405", timeStr, time.Local)
	if err != nil {
		return
	}
	timestamp = t.Unix()
	return
}
