/*
   Copyright 2020 XiaochengTech

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package wxofficial

import (
	"encoding/json"
	"fmt"

	"gitee.com/xiaochengtech/wechat/util"
)

// 网页授权-拉取用户信息(需scope为snsapi_userinfo)
func GetSnsUserInfo(accessToken string, openId string, lang string) (wxRsp GetSnsUserInfoResponse, err error) {
	if len(lang) <= 0 {
		lang = "zh_CN"
	}
	url := fmt.Sprintf("https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s&lang=%s", accessToken, openId, lang)
	body, err := util.HttpGet(url)
	if err != nil {
		return
	}
	err = json.Unmarshal(body, &wxRsp)
	return
}

type GetSnsUserInfoResponse struct {
	OpenId         string   `json:"openid"`          // 用户唯一标识
	Nickname       string   `json:"nickname"`        // 用户的昵称
	Sex            int      `json:"sex"`             // 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
	Province       string   `json:"province"`        // 用户所在省份
	City           string   `json:"city"`            // 用户所在城市
	Country        string   `json:"country"`         // 用户所在国家
	HeadimgUrl     string   `json:"headimgurl"`      // 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。
	Privilege      []string `json:"privilege"`       // 用户特权信息
	UnionId        string   `json:"unionid"`         // 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
	IsSnapshotUser int      `json:"is_snapshotuser"` // 是否为是否为快照页模式虚拟账号，值为0时是普通用户，1时是虚拟帐号
}
