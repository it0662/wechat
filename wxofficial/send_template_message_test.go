/*
   Copyright 2020 XiaochengTech

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package wxofficial

import (
	"fmt"
	"testing"
)

func TestSendTemplateMessage(t *testing.T) {
	fmt.Println("----------发送模板消息----------")
	// 请求接口
	color := "#173177"
	rsp, err := SendTemplateMessage(
		"",
		"",
		"",
		"http://www.baidu.com",
		"#FF0000",
		map[string]SendTemplateMessageItem{
			"first": {
				Value: "车主你好",
				Color: color,
			},
			"keynote1": {
				Value: "京A12380",
				Color: color,
			},
			"keynote2": {
				Value: "杭州停车场",
				Color: color,
			},
			"keynote3": {
				Value: "2小时38分22秒",
				Color: color,
			},
			"keynote4": {
				Value: "20.80元",
				Color: color,
			},
			"keynote5": {
				Value: "2020年08月20日 17:44:23",
				Color: color,
			},
			"remark": {
				Value: "请您于支付后5分钟内开车驶离停车场，否则还需再进行超时部分的停车费扫码补缴。",
				Color: color,
			},
		},
	)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("返回值: %+v\n", rsp)
}
