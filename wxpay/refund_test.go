/*
   Copyright 2020 XiaochengTech

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package wxpay

import (
	"fmt"
	"testing"

	"gitee.com/xiaochengtech/wechat/util"
)

// 测试申请退款
func TestRefund(t *testing.T) {
	fmt.Println("----------申请退款----------")
	outRefundNo := util.RandomString(32)
	// 初始化参数
	body := RefundBody{}
	body.TransactionId = ""
	body.OutTradeNo = ""
	body.OutRefundNo = outRefundNo
	body.TotalFee = 100 // 必须用100
	body.RefundFee = 1
	// 请求申请退款
	wxRsp, err := testClient.Refund(body)
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Printf("返回值: %+v\n", wxRsp)
}
